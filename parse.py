# encoding=utf8
import sys

from __builtin__ import xrange

reload(sys)
sys.setdefaultencoding('utf8')

import json

import datetime
from lxml import etree

def format_date(value):
    return datetime.datetime.strptime(value, "%Y-%m-%dT%H:%M:%S").strftime("%d/%m/%y")

def get_duration(start_date, finish_date):
    fromdate = datetime.datetime.strptime(start_date, "%Y-%m-%dT%H:%M:%S")
    todate = datetime.datetime.strptime(finish_date, "%Y-%m-%dT%H:%M:%S")
    nb_days = (todate - fromdate).days
    daygenerator = (fromdate + datetime.timedelta(x + 1) for x in xrange(nb_days))
    return sum(1 for day in daygenerator if day.weekday() < 5)

tree = etree.parse("./planning.xml")

resources = {}
xml_resources = tree.xpath("/Project/Resources/Resource")
for resource in xml_resources:
    resources[resource.find("UID").text] = {
        "name": resource.find("Name").text,
        "total_cost": 0
    }

assignments = {}
xml_assignments = tree.xpath("/Project/Assignments/Assignment")
for assignment in xml_assignments:
    a = {
        "resource_uid": assignment.find("ResourceUID").text,
        "name": resources[assignment.find("ResourceUID").text]['name'],
        "rate": assignment.find("Units").text
    }

    try:
        assignments[assignment.find("TaskUID").text].append(a)
    except KeyError:
        assignments[assignment.find("TaskUID").text] = [a]

xml_tasks = tree.xpath("/Project/Tasks/Task")
result = {
    "wbs": xml_tasks[0].find("WBS").text,
    "name": xml_tasks[0].find("Name").text,
    "startdate": format_date(xml_tasks[0].find("Start").text),
    "finishdate": format_date(xml_tasks[0].find("Finish").text),
    "infos": xml_tasks[0].find("PercentComplete").text + '%',
    "modules": []
}
tasks = []
for task in xml_tasks:
    uid = task.find("UID").text
    wbs = task.find("WBS").text.split('.')
    name = task.find("Name").text
    infos = task.find("PercentComplete").text + '%'
    start_date = format_date(task.find("Start").text)


    completion = int(task.find("PercentComplete").text)
    finish_date = format_date(task.find("Finish").text)
    is_jalon = start_date == finish_date
    try:
        task_assignment = assignments[uid]
    except KeyError:
        task_assignment = {}

    assignment_names = ""
    for assignment in task_assignment:
        duration = get_duration(task.find("Start").text, task.find("Finish").text) * float(assignment['rate'])
        assignment_names += assignment['name']
        assignment_names += " (" + "{0:g}".format(duration) + " jh)" + " " if duration > 0 else " "
        resources[assignment['resource_uid']]['total_cost'] += duration

        try:
            result['modules'][int(wbs[1]) - 1]['resources'][assignment['resource_uid']]['cost'] += duration
        except KeyError:
            result['modules'][int(wbs[1]) - 1]['resources'][assignment['resource_uid']] = {
                "name": assignment['name'],
                "cost": duration
            }


    if assignment_names != "":
        assignment_names += " | " + infos if infos != "0%" else ""


    if (len(wbs) == 2):
        result['modules'].append({
            "name": name,
            "wbs": task.find("WBS").text,
            "tasks": [],
            "infos": infos,
            "startdate": start_date,
            "finishdate": finish_date,
            "resources": {}
        })

    elif (len(wbs) == 3):
        result['modules'][int(wbs[1]) - 1]['tasks'].append({
            "name": name,
            "wbs": task.find("WBS").text,
            "infos": assignment_names,
            "startdate": start_date,
            "finishdate": finish_date,
            "status": 'todo' if completion == 0 else 'done' if completion == 100 else 'doing',
            "is_jalon": is_jalon
        })

with open('data.json', 'w') as outfile:
    json.dump(result, outfile)

output_html = '''
<!DOCTYPE html>
<html lang="en">
<head><meta charset="UTF-8">
    <title>WBS</title>
    <link rel="stylesheet" href="main.css">
</head>
<body>
    <div class="container">'''

output_html += '''
        <div class="row">
            <div class="column">
                <div class="total_cost">
                    <div>Coût total:</div>'''

for index, resource in resources.iteritems():
    if(resource['total_cost'] > 0):
        output_html += '<div>{name} : {total_cost:g} jh</div>'.format(name=resource['name'], total_cost=resource['total_cost'])

output_html += '''
                </div>
                <div class="block">
                    <div class="name">{name}</div>
                    <div class="bottomelement wbs">{wbs}</div>
                    <div class="bottomelement infos">{infos}</div>
                    <div class="bottomelement startdate">{startdate}</div>
                    <div class="bottomelement finishdate">{finishdate}</div>
                </div>
            </div>
            <div class="connector-1"></div>
            <!--1 => 2px / 2 => 226 / 3 => 450 / 4 => 674 / 5 => 898-->
            <div class="connector-3" style="width:{width}px;"></div>
        </div>
        <div class="row">
'''.format(
    width=(len(result['modules']) - 1) * 284 + 2,
    name=result['name'],
    wbs=result['wbs'],
    infos=result['infos'],
    startdate=result['startdate'],
    finishdate=result['finishdate'])

for module in result['modules']:
    task_resources_cost = '<div class="cost">'
    for index, resource in module['resources'].iteritems():
        if(resource['cost'] > 0):
            task_resources_cost += '<div>{name} : {total_cost:g} jh</div>'.format(name=resource['name'], total_cost=resource['cost'])

    task_resources_cost += '</div>'

    output_html += '''
            <div class="column">
                <div class="block">
                    <div class="connector-2"></div>
                    <div class="connector-4"></div>
                    <!--120 - 20 + 3 * 120 - 80-->
                    <div class="connector-5" style="height:{height}px"></div>
                    <span class="name" title="{name}">{name}</span>
                    {cost}
                    <div class="bottomelement wbs">{wbs}</div>
                    <div class="bottomelement infos">{infos}</div>
                    <div class="bottomelement startdate">{startdate}</div>
                    <div class="bottomelement finishdate">{finishdate}</div>
                </div>
    '''.format(
        height=(120 - 20 + len(module['tasks']) * 120 - 80),
        name=module['name'],
        cost=task_resources_cost,
        wbs=module['wbs'],
        infos=module['infos'],
        startdate=module['startdate'],
        finishdate=module['finishdate'])

    for task in module['tasks']:
        output_html += '''
                <div class="block task">
                    <div class="connector-4"></div>
                    <span class="name" title="{name}">{name}</span>
                    <div class="bottomelement wbs">{wbs}</div>
                    <div class="bottomelement infos">{infos}</div>
                    <div class="bottomelement startdate {is_jalon}">{startdate}</div>
                    <div class="bottomelement finishdate {is_jalon}">{finishdate}</div>
                    <div class="bottomelement status {status}">{status_text}</div>
                </div>'''.format(
            name=task['name'],
            wbs=task['wbs'],
            infos=task['infos'],
            is_jalon="jalon" if task['is_jalon'] else "",
            startdate=task['startdate'],
            finishdate=task['finishdate'],
            status=task['status'],
            status_text={'done': 'Effectué', 'todo': 'À faire', 'doing': 'En cours'}[task['status']])

    output_html += '''
            </div>'''

output_html += '''
        </div>
    </div>
</body>
</html>'''

with open('test.html', 'w') as outfile:
    outfile.write(output_html)
